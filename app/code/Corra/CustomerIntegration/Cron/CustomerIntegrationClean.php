<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Cron;

use Corra\Mulesoft\Model\TableCleaner;
use Corra\CustomerIntegration\Model\Source\SourceData;

/**
 * Class CustomerIntegrationClean
 *
 * @package Corra\CustomerIntegration\Cron
 */
class CustomerIntegrationClean
{
    const OLDER_THAN_DAYS = 30;

    /**
     * @var TableCleaner
     */
    private $tableCleaner;

    /**
     * CustomerIntegrationClean constructor.
     *
     * @param TableCleaner $tableCleaner
     */
    public function __construct(TableCleaner $tableCleaner)
    {
        $this->tableCleaner = $tableCleaner;
    }

    /**
     * Run customer integration clean by cron.
     */
    public function execute()
    {
        $this->tableCleaner->deleteTableData(
            SourceData::TABLE_NAME,
            SourceData::CREATED_AT,
            self::OLDER_THAN_DAYS
        );
    }
}
