<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Cron;

use Corra\CustomerIntegration\Model\CustomerIntegration as CustomerIntegrationModel;

/**
 * Class CustomerIntegration
 *
 * @package Corra\CustomerIntegration\Cron
 */
class CustomerIntegration
{
    /**
     * @var CustomerIntegrationModel
     */
    private $customerIntegration;

    /**
     * CustomerIntegration constructor.
     *
     * @param CustomerIntegrationModel $customerIntegration
     */
    public function __construct(CustomerIntegrationModel $customerIntegration)
    {
        $this->customerIntegration = $customerIntegration;
    }

    /**
     * Run customer integration by cron.
     */
    public function execute()
    {
        $this->customerIntegration->process();
    }
}
