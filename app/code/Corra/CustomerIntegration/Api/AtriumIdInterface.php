<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Api;

/**
 * Interface AtriumIdInterface
 *
 * @package Corra\CustomerIntegration\Api
 */
interface AtriumIdInterface {
    /**
     * GET atrium id.
     *
     * @param string $customerId
     * @return string
     */
    public function getAtriumId($customerId);
}
