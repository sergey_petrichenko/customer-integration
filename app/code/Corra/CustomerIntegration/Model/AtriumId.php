<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model;

use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\Serialize\SerializerInterface;
use Corra\CustomerIntegration\Model\Source\SourceData;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use InvalidArgumentException;

/**
 * Class AtriumId
 *
 * @package Corra\CustomerIntegration\Model
 */
class AtriumId
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var LoggerConnector
     */
    private $logger;

    /**
     * AtriumId constructor.
     *
     * @param CustomerRepository $customerRepository
     * @param SerializerInterface $serializer
     * @param LoggerConnector $logger
     */
    public function __construct(
        CustomerRepository $customerRepository,
        SerializerInterface $serializer,
        LoggerConnector $logger
    ) {
        $this->customerRepository = $customerRepository;
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getAtriumId($customerId)
    {
        try {
            $response = [];
            $response[SourceData::STATUS] = SourceData::ERROR;
            $response[SourceData::MESSAGE] = "Something went wrong";
            $customer = $this->customerRepository->getById($customerId);
            $atriumId = $customer->getCustomAttribute(SourceData::ATRIUM_ID)->getValue();

            if ($atriumId !== null) {
                $response = [];
                $response[SourceData::STATUS] = SourceData::SUCCESS;
                $response[SourceData::ATRIUM_ID] = $atriumId;
            } else {
                $response[SourceData::MESSAGE] = "There is not an atrium id of the customer";
            }
        } catch (NoSuchEntityException $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
            $response[SourceData::MESSAGE] = "There is not a customer";
        } catch (LocalizedException $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
        } catch (InvalidArgumentException $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
        }

        return $this->serializer->serialize($response);
    }
}
