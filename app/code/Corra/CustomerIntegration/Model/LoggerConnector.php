<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model;

use Psr\Log\LoggerInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Corra\CustomerIntegration\Model\Source\SourceData;
use Magento\Framework\Module\Manager;

/**
 * Class Logger
 *
 * @package Corra\CustomerIntegration\Model
 */
class LoggerConnector
{
    /**
     * @var string
     */
    const MESSAGE_KEY = 'message';

    /**
     * @var string
     */
    const ALERT = 'alert';

    /**
     * @var string
     */
    const TRACE = 'trace';

    /**
     * @var string
     */
    const CORRA_LOG = 'Corra_Log';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $logNalert = [
        'logNalert' =>
            [
            'unique_id' => "customer_integration_",
            'message' =>
                [
                'type' => "Customer Integration",
                'subtype' => ""
                ],
            'trace' =>
                [
                'filename' => "customer_integration.log"
                ],
            'alert' => []
            ]
    ];

    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * Logger constructor.
     *
     * @param LoggerInterface $logger
     * @param SerializerInterface $serializer
     * @param Manager $moduleManager
     */
    public function __construct(
        LoggerInterface $logger,
        SerializerInterface $serializer,
        Manager $moduleManager
    ) {
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->moduleManager = $moduleManager;
    }

    /**
     * Prepare logger message before log.
     *
     * @param string $exceptionMessage
     * @param string|null $status
     * @param string|null $trace
     * @param bool $alert
     * @return string
     */
    public function prepareMessage(
        $exceptionMessage,
        $trace = null,
        $status = null,
        $alert = false
    ) {
        $this->message = $exceptionMessage;

        if ($this->moduleManager->isEnabled(self::CORRA_LOG)) {
            $message[self::MESSAGE_KEY] = [
                self::MESSAGE_KEY => $exceptionMessage,
                SourceData::STATUS => $status
            ];

            $this->logNalert['logNalert']['unique_id'] =
                $this->logNalert['logNalert']['unique_id'] . mt_rand() . mt_rand();

            if ($trace) {
                $message[self::TRACE] = $trace;
            }

            if ($alert) {
                $message[self::ALERT] = $exceptionMessage;
            }

            $this->message = $this->serializer->serialize($message);
        }

        return $this;
    }

    /**
     * Send prepared message to log it.
     *
     * @return $this
     */
    public function send()
    {
        if ($this->moduleManager->isEnabled(self::CORRA_LOG)) {
            $this->logger->addInfo($this->message, $this->logNalert);
        } else {
            $this->logger->critical($this->message);
        }

        return $this;
    }
}
