<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model;

use Corra\CustomerIntegration\Model\Source\SourceData;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\Serialize\SerializerInterface;
use Exception;

/**
 * Class ApiRequest
 *
 * @package Corra\CustomerIntegration\Model
 */
class ApiRequest
{
    /**
     * @var string
     */
    const RETRY = 'Retry';

    /**
     * @var string
     */
    const REAL_TIME = 'Real-Time';

    /**
     * @var ApiConnector
     */
    private $apiConnector;

    /**
     * @var LoggerConnector
     */
    private $logger;

    /**
     * @var ZendClient
     */
    private $zendClient;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * ApiRequest constructor.
     *
     * @param ApiConnector $apiConnector
     * @param LoggerConnector $logger
     * @param ZendClient $zendClient
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ApiConnector $apiConnector,
        LoggerConnector $logger,
        ZendClient $zendClient,
        SerializerInterface $serializer
    ) {
        $this->apiConnector = $apiConnector;
        $this->logger = $logger;
        $this->zendClient = $zendClient;
        $this->serializer = $serializer;
    }

    /**
     * Send a request to API.
     *
     * @param $customerId
     * @param $method
     * @param $addressId
     * @param $atriumId
     * @param $count
     * @param $retry
     * @return bool
     */
    public function sendRequest(
        $customerId,
        $method,
        $addressId = null,
        $atriumId = null,
        $count = null,
        $retry = null
    ) {
        try {
            $body = "";
            $apiUri = $this->apiConnector->getApiUri($customerId, $method, $addressId);
            $headers = $this->apiConnector->getApiHeaders();
            $user = $this->apiConnector->getApiLogin();
            $password = $this->apiConnector->getApiPassword();
            $this->zendClient->setUri($apiUri);
            $this->zendClient->setAuth($user, $password, ZendClient::AUTH_BASIC);
            $this->zendClient->setHeaders($headers);

            if ($atriumId) {
                $rawData[SourceData::ATRIUM_ID_API] = $atriumId;
                $body = $this->serializer->serialize($body);
                $this->zendClient->setRawData($body);
            }

            $response = $this->zendClient->request($method);

            if ($this->apiConnector->getIsDebugMode()) {
                $requestMessage = $this->getRequestDebugMessage(
                    $apiUri,
                    $user,
                    $password,
                    $method,
                    $count,
                    $retry,
                    $body
                );
                $this->logMessage($requestMessage);
            }

            $responseMessage = $this->getResponseDebugMessage(
                $response,
                $apiUri,
                $count,
                $retry,
                $method
            );

            if ($responseMessage) {
                $this->logMessage($responseMessage);
            }

            return $this->processResponse($response);
        } catch (Exception $exception) {
            $this->logMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            );
        }
    }

    /**
     * Check that request is executed successfully.
     *
     * @param $response
     * @return bool
     */
    private function processResponse($response)
    {
        $responseBody = $this->serializer->unserialize($response->getBody());
        $success = true;
        $status = '';

        if (is_array($responseBody)) {
            $status = isset($responseBody['status']) ? $responseBody['status'] : '';
        } elseif ($responseBody->status) {
            $status = $responseBody->status;
        }

        if ($status !== ApiConnector::SUCCESS) {
            $success = false;
        }

        return $success;
    }

    /**
     * Log a message.
     *
     * @param $message
     * @param null $trace
     */
    private function logMessage($message, $trace = null)
    {
        $this->logger->prepareMessage(
            $message,
            $trace
        )->send();
    }

    /**
     * Get request debug message.
     *
     * @param $apiUri
     * @param $user
     * @param $password
     * @param $method
     * @param $count
     * @param $retry
     * @param $body
     * @return bool|string
     */
    private function getRequestDebugMessage(
        $apiUri,
        $user,
        $password,
        $method,
        $count,
        $retry,
        $body
    ) {
        $message = [];
        $type = $retry ? self::RETRY : self::REAL_TIME;
        $message['type'] = $type . ' Customer Integration';
        $message['subtype'] = 'Request';
        $message['count'] = $count;
        $message['api_url'] = $apiUri;
        $message['method'] = $method;
        $message['header'] = 'Login: ' . $user . ' ' . 'Password: ' . md5($password);
        $message['body'] = $body;

        return $this->serializer->serialize($message);
    }

    /**
     * Get response debug message.
     *
     * @param $response
     * @param $apiUri
     * @param $count
     * @param $retry
     * @param $method
     * @return bool|string
     */
    private function getResponseDebugMessage(
        $response,
        $apiUri,
        $count,
        $retry,
        $method
    ) {
        $message = "";

        try {
            if ($this->processResponse($response)
                && !$this->apiConnector->getIsDebugMode()
            ) {
                return $message;
            }

            $messageData = [];
            $type = $retry ? self::RETRY : self::REAL_TIME;
            $messageData['type'] = $type;
            $messageData['subtype'] = 'Response';
            $messageData['count'] = $count;
            $messageData['api_url'] = $apiUri;
            $messageData['code'] = $response->getStatus();
            $messageData['method'] = $method;
            $messageData['header'] = $response->getHeaders();
            $messageData['body'] = $response->getBody();
            $message = $this->serializer->serialize($messageData);
        } catch (Exception $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
        }

        return $message;
    }
}
