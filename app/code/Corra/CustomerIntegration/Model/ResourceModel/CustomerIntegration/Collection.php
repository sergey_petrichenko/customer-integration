<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model\ResourceModel\CustomerIntegration;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Corra\CustomerIntegration\Model\CustomerIntegration as CustomerIntegrationModel;
use Corra\CustomerIntegration\Model\ResourceModel\CustomerIntegration
    as CustomerIntegrationResourceModel;

/**
 * Class Collection
 *
 * @package Corra\CustomerIntegration\Model\ResourceModel\CustomerIntegration
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            CustomerIntegrationModel::class,
            CustomerIntegrationResourceModel::class
        );
    }
}
