<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model;

use Magento\Customer\Model\Address;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Registry;
use Magento\Framework\Model\Context;
use Corra\CustomerIntegration\Model\ResourceModel\CustomerIntegration
    as CustomerIntegrationResource;
use Corra\CustomerIntegration\Model\ResourceModel\CustomerIntegration\CollectionFactory;
use Corra\CustomerIntegration\Model\ResourceModel\CustomerIntegration\Collection;
use Magento\Customer\Model\Customer;
use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Corra\CustomerIntegration\Model\Source\SourceData;
use Magento\Framework\HTTP\ZendClient;
use Corra\CustomerIntegration\Helper\Data;

/**
 * Class CustomerIntegration
 *
 * @package Corra\CustomerIntegration\Model
 */
class CustomerIntegration extends AbstractModel
{
    /**
     * @var int
     */
    const COLLECTION_PAGE_SIZE = 1000;

    /**
     * @var int
     */
    const COLLECTION_FIRST_PAGE = 1;

    /**
     * @var CustomerIntegrationResource
     */
    private $resourceModel;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var LoggerConnector
     */
    private $logger;

    /**
     * @var ApiRequest
     */
    private $apiRequest;

    /**
     * @var Data
     */
    private $helper;

    /**
     * CustomerIntegration constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param CustomerIntegrationResource $resourceModel
     * @param CollectionFactory $collectionFactory
     * @param LoggerConnector $logger
     * @param ApiRequest $apiRequest
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        CustomerIntegrationResource $resourceModel,
        CollectionFactory $collectionFactory,
        LoggerConnector $logger,
        ApiRequest $apiRequest,
        Data $helper,
        array $data = []
    ) {
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
        $this->apiRequest = $apiRequest;
        $this->helper = $helper;
        parent::__construct($context, $registry);
    }

    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(CustomerIntegrationResource::class);
    }

    /**
     * Method to process a cron job and executing a console command.
     *
     * @return int
     */
    public function process()
    {
        $this->collection = $this->collectionFactory->create();
        $this->collection->addFieldToFilter(
            SourceData::STATUS,
            SourceData::STATUS_FAILED
        );
        $maxTries = $this->helper->getApiMaxTries();
        $itemsCount = 0;

        try {
            if ($maxTries) {
                $count = $this->collection->count();
                $pages = ceil($count / self::COLLECTION_PAGE_SIZE);
                $this->collection->setPageSize(self::COLLECTION_PAGE_SIZE);
                $itemsCount = $this->processRetry($maxTries, $pages);

                if ($itemsCount) {
                    $message = 'Customer Integration was successfully finished.'
                        . ' '
                        . 'Were processed'
                        . ' '
                        . $itemsCount
                        . ' '
                        . 'rows';
                    $this->logger->prepareMessage(
                        $message,
                        null,
                        SourceData::STATUS_SUCCESS
                    )->send();
                }
            }
        } catch (Exception $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
            $this->collection->getConnection()->rollBack();
        }

        return $itemsCount;
    }

    /**
     * Get customer integration data by id.
     *
     * @param int $entityId
     * @return CustomerIntegrationResource
     */
    public function getCustomerIntegrationById($entityId)
    {
        return $this->resourceModel->load($this, $entityId);
    }

    /**
     * Send request to customers API.
     *
     * @param Customer $customer
     */
    public function sendCustomerRequest(Customer $customer)
    {
        if ($customerId = $customer->getId()) {
            $requestResponse = $this->apiRequest->sendRequest(
                $customer->getId(),
                ZendClient::PUT
            );

            $status = $this->getRequestStatus($requestResponse);
            $this->saveCustomerData($customer, $status);
        }
    }

    /**
     * Send request to addresses API.
     *
     * @param Customer $customer
     * @param Address $address
     * @param string $action
     */
    public function sendAddressRequest(Customer $customer, Address $address, $action)
    {
        $customerId = $customer->getId();
        $addressId = $address->getId();
        $atriumId = $address->getAtriumId();

        if ($customerId && $addressId) {
            $method = $this->getAddressRequestMethod($action);
            $requestResponse = $this->apiRequest->sendRequest(
                $customerId,
                $method,
                $addressId,
                $atriumId
            );

            $status = $this->getRequestStatus($requestResponse);
            $this->saveCustomerAddressData($customer, $address, $action, $status);
        }
    }

    /**
     * Process retry requests.
     *
     * @param int $maxTries
     * @param int $pages
     * @return int $itemsCount
     */
    private function processRetry($maxTries, $pages)
    {
        $itemsCount = 0;
        for ($page = self::COLLECTION_FIRST_PAGE; $page <= $pages; $page++) {
            $this->collection->getConnection()->beginTransaction();
            $this->collection->setCurPage($page);

            foreach ($this->collection as & $item) {
                $itemsCount++;
                $customerId = $item->getCustomerId();
                $method = ZendClient::PUT;
                $addressId = $item->getAddressId();
                $atriumId = $item->getAtriumId();
                $retries = $item->getRetries();

                if ($addressId) {
                    $method = $this->getAddressRequestMethod($item->getAction());
                }

                $requestResponse = $this->apiRequest->sendRequest(
                    $customerId,
                    $method,
                    $addressId,
                    $atriumId,
                    $retries,
                    true
                );

                $status = $this->getRequestStatus($requestResponse);

                if ($status === SourceData::STATUS_FAILED) {
                    $itemsCount--;
                    $retries++;
                    $item->setRetries($retries);
                }

                if ($retries >= $maxTries) {
                    $status = SourceData::STATUS_PERMANENTLY_FAILED;
                    $message = 'Customer Integration'
                        . ' '
                        . ucfirst($item->getType())
                        . ' '
                        . 'request of item with entity_id='
                        . $item->getId()
                        . ' '
                        . 'was failed during'
                        . ' '
                        . $maxTries
                        . ' '
                        . 'attempts';
                    $this->logger->prepareMessage(
                        $message,
                        null,
                        $status,
                        true
                    )->send();
                }

                $item->setStatus($status);
            }

            $this->collection->save();
            $this->collection->getConnection()->commit();
        }

        return $itemsCount;
    }

    /**
     * Get an address api request method.
     *
     * @param string $action
     * @return string
     */
    private function getAddressRequestMethod($action)
    {
        switch ($action) {
            case SourceData::ACTION_EDIT:
                $method = ZendClient::PUT;
                break;
            case SourceData::ACTION_DELETE:
                $method = ZendClient::DELETE;
                break;
            default:
                $method = ZendClient::POST;
                break;
        }

        return $method;
    }

    /**
     * Save customer data to DB.
     *
     * @param Customer $customer
     * @param string $status
     * @return $this
     */
    private function saveCustomerData(Customer $customer, $status)
    {
        $this->setCustomerData($customer);
        $this->setData(SourceData::TYPE, SourceData::TYPE_CUSTOMER);
        $this->setData(SourceData::STATUS, $status);
        $this->saveCustomerIntegration();

        return $this;
    }

    /**
     * Save customer address data to DB.
     *
     * @param Customer $customer
     * @param Address $address
     * @param string $action
     * @param string $status
     * @return $this
     */
    private function saveCustomerAddressData(
        Customer $customer,
        Address $address,
        $action,
        $status
    ) {
        $this->setCustomerData($customer);
        $this->setData(SourceData::TYPE, SourceData::TYPE_ADDRESS);
        $this->setData(SourceData::ADDRESS_ID, $address->getId());
        $this->setData(SourceData::ATRIUM_ID, $address->getAtriumId());
        $this->setData(SourceData::ACTION, $action);
        $this->setData(SourceData::STATUS, $status);
        $this->saveCustomerIntegration();

        return $this;
    }

    /**
     * Set customer data before save.
     *
     * @param Customer $customer
     * @return $this
     */
    private function setCustomerData(Customer $customer)
    {
        $this->setData(SourceData::CUSTOMER_ID, $customer->getId());

        return $this;
    }

    /**
     * Save all customer integration data to DB.
     *
     * @return $this
     */
    private function saveCustomerIntegration()
    {
        try {
            $this->resourceModel->save($this);
        } catch (AlreadyExistsException $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
        } catch (Exception $exception) {
            $this->logger->prepareMessage(
                $exception->getMessage(),
                $exception->getTraceAsString()
            )->send();
        }

        return $this;
    }

    /**
     * Get request status based on response.
     *
     * @param bool $requestResponse
     * @return string
     */
    private function getRequestStatus($requestResponse)
    {
        return $requestResponse === true
            ? SourceData::STATUS_SUCCESS
            : SourceData::STATUS_FAILED;
    }
}
