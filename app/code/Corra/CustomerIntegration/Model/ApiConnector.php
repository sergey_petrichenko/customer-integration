<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model;

use Corra\CustomerIntegration\Helper\Data;
use Magento\Framework\HTTP\ZendClient;

class ApiConnector
{
    /**
     * @var string
     */
    const SUCCESS = 'SUCCESS';

    /**
     * @var string
     */
    const CUSTOMER_ID_PATTERN = '{customerId}';

    /**
     * @var string
     */
    const ADDRESS_ID_PATTERN = '{addressId}';

    /**
     * @var string
     */
    const CONTENT_TYPE_HEADER = 'Content-Type: application/json';

    /**
     * @var Data
     */
    private $helper;

    /**
     * ApiConnector constructor.
     *
     * @param Data $helper
     */
    public function __construct(Data $helper) {
        $this->helper = $helper;
    }

    /**
     * Create and get api headers.
     *
     * @return array
     */
    public function getApiHeaders()
    {
        $headers = [
            self::CONTENT_TYPE_HEADER
        ];

        return $headers;
    }

    /**
     * Get appropriate api request url.
     *
     * @param int $customerId
     * @param null|string $method
     * @param null|int $addressId
     * @return string $apiUri
     */
    public function getApiUri($customerId, $method, $addressId = null)
    {
        $apiUri = $this->helper->getCustomerApiUrl();

        if ($addressId) {
            if ($method === ZendClient::PUT) {
                $apiUri = $this->helper->getPutAddressApiUrl();
            } elseif ($method === ZendClient::POST) {
                $apiUri = $this->helper->getPostAddressApiUrl();
            } elseif ($method === ZendClient::DELETE) {
                $apiUri = $this->helper->getDeleteAddressApiUrl();
            }
        }

        $apiUri = $apiUri ? ltrim($apiUri, '/') : '';
        $apiUri = str_replace(self::CUSTOMER_ID_PATTERN, $customerId, $apiUri);
        $apiUri = str_replace(self::ADDRESS_ID_PATTERN, $addressId, $apiUri);
        $apiUri = $this->getApiHost() . '/' . $apiUri;

        return $apiUri;
    }

    /**
     * Get api login for stage or sandbox account.
     *
     * @return string $login
     */
    public function getApiLogin()
    {
        $login = $this->helper->getStageApiLogin();

        if ($this->helper->getUseSandboxApi()) {
            $login = $this->helper->getSandboxApiLogin();
        }

        $login = $login ?: '';

        return $login;
    }

    /**
     * Get api password for stage or sandbox account.
     *
     * @return string $password
     */
    public function getApiPassword()
    {
        $password = $this->helper->getStageApiPassword();

        if ($this->helper->getUseSandboxApi()) {
            $password = $this->helper->getSandboxApiPassword();
        }

        $password = $password ?: '';

        return $password;
    }

    /**
     * Get api host for stage or sandbox account.
     *
     * @return string $host
     */
    private function getApiHost()
    {
        $host = $this->helper->getStageApiHost();

        if ($this->helper->getUseSandboxApi()) {
            $host = $this->helper->getSandboxApiHost();
        }

        $host = $host ? rtrim($host, '/') : '';

        return $host;
    }

    /**
     * Get is debug mode enabled.
     *
     * @return mixed
     */
    public function getIsDebugMode()
    {
        return $this->helper->getEnableCustomerIntegrationDebugLogging();
    }
}
