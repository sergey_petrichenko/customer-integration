<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Model\Source;

class SourceData
{
    /**
     * @var string
     */
    const TABLE_NAME = 'corra_customer_integration';

    /**
     * @var string
     */
    const ID_FIELD_NAME = 'entity_id';

    /**
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * @var string
     */
    const STATUS_FAILED = 'failed';

    /**
     * @var string
     */
    const STATUS_PERMANENTLY_FAILED = 'permanently_failed';

    /**
     * @var string
     */
    const STATUS_SUCCESS = 'success';

    /**
     * @var string
     */
    const CUSTOMER_ID = 'customer_id';

    /**
     * @var string
     */
    const ATRIUM_ID = 'atrium_id';

    /**
     * @var string
     */
    const ATRIUM_ID_API = 'atriumId';

    /**
     * @var string
     */
    const ADDRESS_ID = 'address_id';

    /**
     * @var string
     */
    const RETRIES = 'retries';

    /**
     * @var string
     */
    const STATUS = 'status';

    /**
     * @var string
     */
    const TYPE_CUSTOMER = 'customer';

    /**
     * @var string
     */
    const TYPE_ADDRESS = 'address';

    /**
     * @var string
     */
    const ROW_STATUS_DEFAULT = 'new';

    /**
     * @var string
     */
    const ROW_STATUS_EXISTED = 'existed';

    /**
     * @var string
     */
    const TYPE = 'type';

    /**
     * @var string
     */
    const ACTION = 'action';

    /**
     * @var string
     */
    const ACTION_ADD = 'add';

    /**
     * @var string
     */
    const ACTION_EDIT = 'edit';

    /**
     * @var string
     */
    const ACTION_DELETE = 'delete';

    /**
     * @var string
     */
    const ERROR = 'ERROR';

    /**
     * @var string
     */
    const SUCCESS = 'SUCCESS';

    /**
     * @var string
     */
    const MESSAGE = 'message';

    /**
     * @var string
     */
    const CHANGE_SYSTEM = 'magento';
}
