<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Console\Command;

use Symfony\Component\Console\Command\Command;
use Corra\CustomerIntegration\Model\CustomerIntegration;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunCustomerIntegrationCronJob extends Command
{
    /**
     * @var CustomerIntegration
     */
    private $customerIntegration;

    /**
     * @param CustomerIntegration $customerIntegration
     */
    public function __construct(CustomerIntegration $customerIntegration)
    {
        $this->customerIntegration = $customerIntegration;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('customer:integration:cron:run')
            ->setDescription('Run the customer integration cron job');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $output->writeln('<info>Customer Integration Cron Job is started</info>');
        $itemsCount = $this->customerIntegration->process();
        $output->writeln('<info>Customer Integration Cron Job is finished</info>');
        $output->writeln('<info>Were processed ' . $itemsCount . ' rows</info>');
    }
}
