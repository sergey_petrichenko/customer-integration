<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Console\Command;

use Symfony\Component\Console\Command\Command;
use Corra\CustomerIntegration\Cron\CustomerIntegrationClean;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunCustomerIntegrationCleanCronJob extends Command
{
    /**
     * @var CustomerIntegrationClean
     */
    private $customerIntegrationClean;

    /**
     * @param CustomerIntegrationClean $customerIntegrationClean
     */
    public function __construct(
        CustomerIntegrationClean $customerIntegrationClean
    ) {
        $this->customerIntegrationClean = $customerIntegrationClean;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('customer:integration:clean:cron:run')
            ->setDescription('Run the customer integration clean cron job');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->setDecorated(true);
        $output->writeln('<info>Customer Integration Clean Cron Job is started</info>');
        $this->customerIntegrationClean->execute();
        $output->writeln('<info>Customer Integration Clean Cron Job is finished</info>');
    }
}
