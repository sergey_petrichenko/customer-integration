<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Class Data
 *
 * @package Corra\CustomerIntegration\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var string
     */
    const ENABLE_CUSTOMER_INTEGRATION_PATH =
        'corra_customer_integration/general/enable_corra_customer_integration';

    /**
     * @var string
     */
    const ENABLE_CUSTOMER_INTEGRATION_DEBUG_LOGGING_PATH =
        'corra_customer_integration/general/enable_corra_customer_integration_debug_logging';

    /**
     * @var string
     */
    const API_MAX_TRIES = 'corra_customer_integration/api/max_tries';

    /**
     * @var string
     */
    const STAGE_API_HOST_PATH = 'corra_customer_integration/api/stage_host';

    /**
     * @var string
     */
    const STAGE_API_LOGIN_PATH = 'corra_customer_integration/api/stage_api_login';

    /**
     * @var string
     */
    const STAGE_API_PASS_PATH = 'corra_customer_integration/api/stage_api_pass';

    /**
     * @var string
     */
    const SANDBOX_API_HOST_PATH = 'corra_customer_integration/api/sandbox_host';

    /**
     * @var string
     */
    const SANDBOX_API_LOGIN_PATH = 'corra_customer_integration/api/sandbox_api_login';

    /**
     * @var string
     */
    const SANDBOX_API_PASS_PATH = 'corra_customer_integration/api/sandbox_api_pass';

    /**
     * @var string
     */
    const USE_SANDBOX_API_PATH = 'corra_customer_integration/api/use_sandbox';

    /**
     * @var string
     */
    const CUSTOMER_API_URL_PATH = 'corra_customer_integration/api/customer_api_url';

    /**
     * @var string
     */
    const POST_ADDRESS_API_URL_PATH = 'corra_customer_integration/api/post_address_api_url';

    /**
     * @var string
     */
    const PUT_ADDRESS_API_URL_PATH = 'corra_customer_integration/api/put_address_api_url';

    /**
     * @var string
     */
    const DELETE_ADDRESS_API_URL_PATH = 'corra_customer_integration/api/delete_address_api_url';

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        Context $context,
        EncryptorInterface $encryptor
    ) {
        $this->encryptor = $encryptor;
        parent::__construct($context);
    }

    /**
     * Get enable corra customer integration config value.
     *
     * @return mixed
     */
    public function getEnableCustomerIntegration()
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_CUSTOMER_INTEGRATION_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get enable corra customer integration debug logging config value.
     *
     * @return mixed
     */
    public function getEnableCustomerIntegrationDebugLogging()
    {
        return $this->scopeConfig->getValue(
            self::ENABLE_CUSTOMER_INTEGRATION_DEBUG_LOGGING_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get use sandbox api credentials config value.
     *
     * @return mixed
     */
    public function getUseSandboxApi()
    {
        return $this->scopeConfig->getValue(
            self::USE_SANDBOX_API_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get stage api host config value.
     *
     * @return mixed
     */
    public function getStageApiHost()
    {
        return $this->scopeConfig->getValue(
            self::STAGE_API_HOST_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get stage api login config value.
     *
     * @return mixed
     */
    public function getStageApiLogin()
    {
        return $this->scopeConfig->getValue(
            self::STAGE_API_LOGIN_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get stage api password config value.
     *
     * @return mixed
     */
    public function getStageApiPassword()
    {
        $stagePassword = $this->scopeConfig->getValue(
            self::STAGE_API_PASS_PATH,
            ScopeInterface::SCOPE_STORE
        );

        if ($stagePassword) {
            $stagePassword = $this->encryptor->decrypt($stagePassword);
        }

        return $stagePassword;
    }

    /**
     * Get sandbox api host config value.
     *
     * @return mixed
     */
    public function getSandboxApiHost()
    {
        return $this->scopeConfig->getValue(
            self::SANDBOX_API_HOST_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get sandbox api login config value.
     *
     * @return mixed
     */
    public function getSandboxApiLogin()
    {
        return $this->scopeConfig->getValue(
            self::SANDBOX_API_LOGIN_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get sandbox api password config value.
     *
     * @return mixed
     */
    public function getSandboxApiPassword()
    {
        $sandboxPassword = $this->scopeConfig->getValue(
            self::SANDBOX_API_PASS_PATH,
            ScopeInterface::SCOPE_STORE
        );

        if ($sandboxPassword) {
            $sandboxPassword = $this->encryptor->decrypt($sandboxPassword);
        }

        return $sandboxPassword;
    }

    /**
     * Get customer api url config value.
     *
     * @return mixed
     */
    public function getCustomerApiUrl()
    {
        return $this->scopeConfig->getValue(
            self::CUSTOMER_API_URL_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get post address api url config value.
     *
     * @return mixed
     */
    public function getPostAddressApiUrl()
    {
        return $this->scopeConfig->getValue(
            self::POST_ADDRESS_API_URL_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get put address api url config value.
     *
     * @return mixed
     */
    public function getPutAddressApiUrl()
    {
        return $this->scopeConfig->getValue(
            self::PUT_ADDRESS_API_URL_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get delete address api url config value.
     *
     * @return mixed
     */
    public function getDeleteAddressApiUrl()
    {
        return $this->scopeConfig->getValue(
            self::DELETE_ADDRESS_API_URL_PATH,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get api max tries url config value.
     *
     * @return int
     */
    public function getApiMaxTries()
    {
        return (int) $this->scopeConfig->getValue(
            self::API_MAX_TRIES,
            ScopeInterface::SCOPE_STORE
        );
    }
}
