<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Corra\CustomerIntegration\Model\Source\SourceData;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 *
 * @package Corra\Log\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var int
     */
    const DEFAULT_FOR_RETRIES = 0;

    /**
     * Install schema.
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $table = $setup->getConnection()->newTable(
            $setup->getTable(SourceData::TABLE_NAME)
        )->addColumn(
            SourceData::ID_FIELD_NAME,
            Table::TYPE_INTEGER,
            null,
            [
                'identity' => true,
                'unsigned' => true,
                'nullable' => false,
                'primary' => true
            ],
            'ID'
        )->addColumn(
            'customer_id',
            Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false
            ],
            'Customer ID'
        )->addColumn(
            'retries',
            Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => false,
                'default' => self::DEFAULT_FOR_RETRIES
            ],
            'Retries'
        )->addColumn(
            'type',
            Table::TYPE_TEXT,
            32,
            [
                'nullable' => false,
            ],
            'Type'
        )->addColumn(
            'address_id',
            Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => true
            ],
            'Address Id'
        )->addColumn(
            'atrium_id',
            Table::TYPE_INTEGER,
            null,
            [
                'unsigned' => true,
                'nullable' => true
            ],
            'Atrium Id'
        )->addColumn(
            'action',
            Table::TYPE_TEXT,
            32,
            [
                'nullable' => true
            ],
            'Action'
        )->addColumn(
            'additional_data',
            Table::TYPE_TEXT,
            null,
            [
                'nullable' => true
            ],
            'Additional Data'
        )->addColumn(
            SourceData::CREATED_AT,
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT
            ],
            'Created At'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT_UPDATE
            ],
            'Updated At'
        )->addColumn(
            'status',
            Table::TYPE_TEXT,
            32,
            ['nullable' => true],
            'Status'
        );

        $setup->getConnection()->createTable($table);
    }
}
