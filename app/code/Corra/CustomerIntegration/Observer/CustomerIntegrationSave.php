<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Observer;

use Corra\CustomerIntegration\Model\Source\SourceData;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Corra\CustomerIntegration\Model\CustomerIntegration;
use Corra\CustomerIntegration\Helper\Data;

/**
 * Class CustomerIntegrationSave
 *
 * @package Corra\CustomerIntegration\Observer
 */
class CustomerIntegrationSave implements ObserverInterface
{
    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var CustomerIntegration
     */
    private $customerIntegration;

    /**
     * @var Data
     */
    private $helper;

    /**
     * CustomerIntegrationSave constructor.
     *
     * @param CustomerSession $customerSession
     * @param CustomerIntegration $customerIntegration
     * @param Data $helper
     */
    public function __construct(
        CustomerSession $customerSession,
        CustomerIntegration $customerIntegration,
        Data $helper
    ) {
        $this->customerSession = $customerSession;
        $this->customerIntegration = $customerIntegration;
        $this->helper = $helper;
    }

    /**
     * Observer method on customer save.
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->customerSession->isLoggedIn()
            && $this->helper->getEnableCustomerIntegration()
        ) {
            $customer = $this->customerSession->getCustomer();
            if ($customer->getChangeSystem() === SourceData::CHANGE_SYSTEM) {
                $this->customerIntegration->sendCustomerRequest($customer);
            }
        }
    }
}
