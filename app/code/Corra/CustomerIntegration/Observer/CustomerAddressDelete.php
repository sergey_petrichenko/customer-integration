<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Observer;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Corra\CustomerIntegration\Model\CustomerIntegration;
use Corra\CustomerIntegration\Model\Source\SourceData;
use Corra\CustomerIntegration\Helper\Data;

/**
 * Class CustomerIntegrationSave
 *
 * @package Corra\CustomerIntegration\Observer
 */
class CustomerAddressDelete implements ObserverInterface
{
    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var CustomerIntegration
     */
    private $customerIntegration;

    /**
     * @var Data
     */
    private $helper;

    /**
     * CustomerIntegrationSave constructor.
     *
     * @param CustomerSession $customerSession
     * @param CustomerIntegration $customerIntegration
     * @param Data $helper
     */
    public function __construct(
        CustomerSession $customerSession,
        CustomerIntegration $customerIntegration,
        Data $helper
    ) {
        $this->customerSession = $customerSession;
        $this->customerIntegration = $customerIntegration;
        $this->helper = $helper;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->customerSession->isLoggedIn()
            && $this->helper->getEnableCustomerIntegration()
        ) {
            $customer = $this->customerSession->getCustomer();
            $customerAddress = $observer->getEvent()->getCustomerAddress();
            if ($customerAddress->getChangeSystem() === SourceData::CHANGE_SYSTEM) {
                $this->customerIntegration->sendAddressRequest(
                    $customer,
                    $customerAddress,
                    SourceData::ACTION_DELETE
                );
            }
        }
    }
}
