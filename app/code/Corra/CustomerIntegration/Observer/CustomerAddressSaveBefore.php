<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\CustomerIntegration\Observer;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;
use Corra\CustomerIntegration\Model\LoggerConnector;
use Corra\CustomerIntegration\Helper\Data;
use Magento\Eav\Model\Entity;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class CustomerIntegrationSave
 *
 * @package Corra\CustomerIntegration\Observer
 */
class CustomerAddressSaveBefore implements ObserverInterface
{
    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var LoggerConnector
     */
    private $logger;

    /**
     * @var Data
     */
    private $helper;

    /**
     * CustomerIntegrationSave constructor.
     *
     * @param CustomerSession $customerSession
     * @param CollectionFactory $collectionFactory
     * @param LoggerConnector $logger
     * @param Data $helper
     */
    public function __construct(
        CustomerSession $customerSession,
        CollectionFactory $collectionFactory,
        LoggerConnector $logger,
        Data $helper
    ) {
        $this->customerSession = $customerSession;
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
        $this->helper = $helper;
    }

    /**
     * Check that a customer edits or adds an address.
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->customerSession->isLoggedIn()
            && $this->helper->getEnableCustomerIntegration()
        ) {
            try {
                $this->customerSession->unsAddressChanged();
                $this->customerSession->unsAddressNewAction();
                $customerAddress = $observer->getEvent()->getCustomerAddress();

                $savedAddress = $this->collectionFactory->create()->addAttributeToFilter(
                    Entity::DEFAULT_ENTITY_ID_FIELD,
                    $customerAddress->getId()
                );

                if (!empty($savedAddress->getData())) {
                    if ($this->hasDataChanges($savedAddress, $customerAddress)) {
                        $this->customerSession->setAddressChanged(true);
                    }
                } else {
                    $this->customerSession->setAddressNewAction(true);
                }
            } catch (LocalizedException $exception) {
                $this->logger->prepareMessage(
                    $exception->getMessage(),
                    $exception->getTraceAsString()
                )->send();
            }
        }
    }

    /**
     * Check that an address data was changed.
     *
     * @param $savedAddress
     * @param $customerAddress
     * @return bool
     */
    private function hasDataChanges($savedAddress, $customerAddress)
    {
        $savedAddressData = $savedAddress->getData();
        $savedAddressData = array_pop($savedAddressData);

        foreach ($savedAddressData as $field => $value) {
            if ($customerAddress->getDataByKey($field) != $value) {
                return true;
            }
        }

        return false;
    }
}
