<?php
/**
 * Copyright © 2018 CORRA. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Corra\Mulesoft\Model;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Adapter\Pdo\Mysql;
use Magento\Framework\Intl\DateTimeFactory;
use DateIntervalFactory;

/**
 * Class TableCleaner
 *
 * @package Corra\Mulesoft\Model
 */
class TableCleaner
{
    /**
     * @var ResourceConnection
     */
    private $resource;

    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;

    /**
     * @var DateIntervalFactory
     */
    private $dateIntervalFactory;

    /**
     * TableCleaner constructor.
     *
     * @param ResourceConnection $resource
     * @param DateTimeFactory $dateTimeFactory
     * @param DateIntervalFactory $dateIntervalFactory
     */
    public function __construct(
        ResourceConnection $resource,
        DateTimeFactory $dateTimeFactory,
        DateIntervalFactory $dateIntervalFactory
    ) {
        $this->resource = $resource;
        $this->dateTimeFactory = $dateTimeFactory;
        $this->dateIntervalFactory = $dateIntervalFactory;
    }

    /**
     * Delete table data that older than days number.
     *
     * @param $tableName
     * @param $columnName
     * @param $days
     */
    public function deleteTableData($tableName, $columnName, $days)
    {
        $connection = $this->resource->getConnection();
        if ($connection->isTableExists($tableName)) {
            $date = $this->dateTimeFactory->create();
            $date->sub($this->dateIntervalFactory->create(
                    [
                        'interval_spec' => sprintf('P%sD', $days)
                    ]
                )
            );
            $dateResult = $date->format(Mysql::TIMESTAMP_FORMAT);
            $connection->delete($tableName, "$columnName < '$dateResult'");
        }
    }
}
